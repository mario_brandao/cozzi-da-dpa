'use strict';

var gulp        = require('gulp');
var plugin      = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var plumber     = require('gulp-plumber');
var pagespeed   = require('psi');
var reload      = browserSync.reload;
var publicUrl   = '';
var minifyCSS   = require('gulp-minify-css');
var uglify      = require('gulp-uglify');
var concat      = require('gulp-concat');
var htmlmin = require('gulp-htmlmin');

var paths = {
  build: 'compressed/',
};

var AUTOPREFIXER_BROWSERS = [
'ie >= 8',
'ie_mob >= 10',
'ff >= 15',
'chrome >= 30',
'safari >= 5',
'opera >= 23',
'ios >= 6',
'android >= 2.3',
'bb >= 10'
];

// Transpile SCSS
gulp.task('styles', function () {
  return gulp.src(['resources/assets/sass/**/*.scss'])
    .pipe(plumber(function (error) {
      console.log("Error happend!", error.message);
      this.emit('end');
    }))
    .pipe(plugin.changed('styles', { extension: '.scss' }))
    .pipe(plugin.sass().on('error', console.error.bind(console)))
    .pipe(plugin.autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe(plumber.stop())
    .pipe(minifyCSS())
    .pipe(gulp.dest('public/css'));
});

// Minify JS
gulp.task('compress-js', function() {
  gulp.src(['resources/assets/js/*.js', 'resources/assets/js/**/*.js'])
    .pipe(uglify())
    .pipe(gulp.dest('public/js/'))
});

// Watch for reload
gulp.task('serve', ['styles', 'compress-js'], function () {
  browserSync({
    notify: false,
    proxy: 'localhost/'
  });

  gulp.watch(['public/img/*'], reload);
  gulp.watch(['resources/assets/sass/**/*.scss'], ['styles']);
  gulp.watch(['public/css/**/*.css'], reload);
  gulp.watch(['resources/assets/js/**/*.js'], ['compress-js']);
  gulp.watch(['public/js/**/*.js'], reload);
});
