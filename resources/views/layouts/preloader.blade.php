<style>
  @-webkit-keyframes preloader{ from {left: 0;width: 0;} 30% {width: 50%;} to {left: 100%;width: 0;} }
  @-moz-keyframes preloader{ from {left: 0;width: 0;} 30% {width: 50%;} to {left: 100%;width: 0;} }
  @-o-keyframes preloader{ from {left: 0;width: 0;} 30% {width: 50%;} to {left: 100%;width: 0;} }
  @keyframes preloader{ from {left: 0;width: 0;} 30% {width: 50%;} to {left: 100%;width: 0;} }
  .screen-reader{display: none;}
  .preloader{z-index: 999999;position: fixed;top: 0;left: 0;width: 100%;height: 100%;background: white;overflow: hidden;}
  .preloader::before{
    content: '';
    z-index: 999999;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 10px;
    background: #a7223a;
    animation: preloader 2s infinite;
  }
</style>