<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title itemprop="name">@yield('title')</title>
    <link rel="canonical"             href="{{ url('/') }}" itemprop="url">
    <meta name="author"               content="DA-DPA">
    <meta name="description"          content="">
    {{-- twitter card --}}
    <meta name="twitter:card"         content="summary">
    <meta name="twitter:image"        content="">
    <meta name="twitter:title"        content="">
    <meta name="twitter:description"  content="">
    <meta name="twitter:creator"      content="DA-DPA">
    {{-- open graph --}}
    <meta property="og:locale"        content="">
    <meta property="og:type"          content="website">
    <meta property="og:title"         content="">
    <meta property="og:description"   content="">
    <meta property="og:url"           content="{{ url('/') }}">
    <meta property="og:image"         content="{{ url('img/meta/ms-icon-310x310.png') }}">
    <meta property="og:image:width"   content="300">
    <meta property="og:image:height"  content="300">
    {{-- META TAGS --}}

    {{-- HOMESCREEN CHROME AND ANDROID --}}
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="{{ url('/') }}">

    {{-- HOMESCREEN SAFARI AND iOS --}}
    <meta name="apple-mobile-web-app-capable"          content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="#fff">
    <link rel="apple-touch-icon" sizes="57x57"    href="{{ url('img/meta/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60"    href="{{ url('img/meta/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72"    href="{{ url('img/meta/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76"    href="{{ url('img/meta/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114"  href="{{ url('img/meta/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120"  href="{{ url('img/meta/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144"  href="{{ url('img/meta/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152"  href="{{ url('img/meta/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180"  href="{{ url('img/meta/apple-icon-180x180.png') }}">
    
    {{-- TITLE FOR WIN8 --}}
    <meta name="msapplication-TileImage" content="{{ url('img/meta/ms-icon-144x144.png') }}">
    <meta name="msapplication-TileColor" content="#fff">
    <meta name="theme-color"             content="#fff">
    <link rel="manifest" href="{{ url('img/meta/manifest.json') }}">
    
    {{-- FAVICON --}}
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('img/meta/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32"   href="{{ url('img/meta/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96"   href="{{ url('img/meta/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16"   href="{{ url('img/meta/favicon-16x16.png') }}">
    <link rel="shortcut icon"                         href="{{ url('img/meta/favicon-16x16.png') }}">
    <link rel="icon"                                  href="{{ url('img/meta/favicon-16x16.png') }}">

    @include('layouts.preloader')
  </head>
  <body>
    <nav class="screen-reader">
      <a href="#content" accesskey="c">Alt + Shift + C ir para o conteúdo</a>
      <a href="#nav" accesskey="m">Alt + Shift + M ir para o menu</a>
      <a href="#footer" accesskey="f">Alt + Shift + F ir para o rodapé</a>
    </nav>
    <div class="preloader"></div>

    <header>
      <div class="header-wrapper">
        <a href="{{ url('/') }}" class="logo">
          <img src="{{ url('public/img/logo-cozzi.svg') }}" alt="Cozzi - Restaurantes Industriais">
        </a>
  
        <button class="btn-menu js-menu-toggle only-mobile">
          <div class="btn-menu-box">
            <div class="btn-menu-inner"></div>
          </div>
        </button>
  
        <div class="menu-content">
          <nav id="nav">
            <ul>
              <li>
                <a href="{{ url('/') }}" title="Início" class="active">Início</a>
              </li>
              <li>
                <a href="{{ url('/') }}" title="Produtos">Produtos</a>
              </li>
              <li>
                <a href="{{ url('/') }}" title="Clientes">Clientes</a>
              </li>
              <li>
                <a href="{{ url('/') }}" title="Restaurantes">Restaurantes</a>
              </li>
              <li>
                <a href="{{ url('/') }}" title="Fornecedores">Fornecedores</a>
              </li>
              <li>
                <a href="{{ url('/') }}" title="Notícias">Notícias</a>
              </li>
              <li>
                <a href="{{ url('/') }}" title="Gastronomia">Gastronomia</a>
              </li>
              <li>
                <a href="{{ url('/') }}" title="Contato">Contato</a>
              </li>
              <li class="social">
                <a href="#" class="facebook" title="Facebook"><i class="fab fa-facebook"></i></a>
              </li>
              <li class="social">
                <a href="#" class="instagram" title="Instagram"><i class="fab fa-instagram"></i></a>
              </li>
            </ul>
          </nav>
  
          <div class="da-dpa-sign">
            <a href="#" target="_blank" title="Feito por: da-dpa">da-dpa</a>
          </div>
        </div>
      </div>
    </header>

    <div id="content" class="content">

      @yield('content')

    </div>

    <footer id="footer">
      <div class="footer-top">
        <div class="container">
          <div class="row">
  
            <div class="facebook-widget wow fadeInUp">
              <h3 class="title">Curta nossa página no <b>Facebook</b></h3>
              <!-- widget -->
              <div class="fb-page" data-href="https://www.facebook.com/cozzirestaurantes/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                <blockquote cite="https://www.facebook.com/cozzirestaurantes/" class="fb-xfbml-parse-ignore">
                  <a href="https://www.facebook.com/cozzirestaurantes/">Cozzi Restaurantes Industriais</a>
                </blockquote>
              </div>
              <!-- widget -->
            </div>
  
            <div class="instagram-widget wow fadeInUp">
              <h3 class="title">Siga-nos no <b>Instagram</b></h3>
  
              <div class="owl-carousel">
                <a href="{{ url('/') }}" class="item">
                  <img src="{{ url('public/img/instagram/pic-01.jpg') }}" alt="Foto 01">
                </a>
                
                <a href="{{ url('/') }}" class="item">
                  <img src="{{ url('public/img/instagram/pic-02.jpg') }}" alt="Foto 01">
                </a>
                <a href="{{ url('/') }}" class="item">
                  <img src="{{ url('public/img/instagram/pic-03.jpg') }}" alt="Foto 01">
                </a>
                <a href="{{ url('/') }}" class="item">
                  <img src="{{ url('public/img/instagram/pic-04.jpg') }}" alt="Foto 01">
                </a>
                <a href="{{ url('/') }}" class="item">
                  <img src="{{ url('public/img/instagram/pic-01.jpg') }}" alt="Foto 01">
                </a>
                <a href="{{ url('/') }}" class="item">
                  <img src="{{ url('public/img/instagram/pic-02.jpg') }}" alt="Foto 01">
                </a>
                <a href="{{ url('/') }}" class="item">
                  <img src="{{ url('public/img/instagram/pic-04.jpg') }}" alt="Foto 01">
                </a>
                <a href="{{ url('/') }}" class="item">
                  <img src="{{ url('public/img/instagram/pic-04.jpg') }}" alt="Foto 01">
                </a>
              </div>
            </div>
  
          </div>
        </div>
      </div>

      <div class="footer-bottom">
        <div class="container">
          <div class="row">

            <div class="phones wow fadeInUp">
              <i class="fa fa-phone"></i>
              <a href="tel:+558134427132">81 3442.7132</a>
              <a href="tel:+996155896">9 9615.5896</a>
              <a href="tel:+5581999921239">9 9992.1239</a>
              <a href="tel:+5581992815331">9 9281.5331</a>
            </div>

            <div class="email wow fadeInUp">
              <a href="mailto:contato@cozzirestaurantes.com.br">
                <i class="fa fa-envelope"></i>
                <b>contato@cozzirestaurantes.com.br</b>
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>

    @yield('assets')

    <!-- Facebook SDK -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.12';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

  </body>
</html>