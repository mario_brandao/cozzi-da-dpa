@extends('layouts.site')

@section('title', 'Cozzi - Restaurantes Industriais')


@section('content')

  <div id="home" class="home">

    <section class="banner">
      <div class="parallax-banner" data-parallax="scroll" data-image-src="{{ url('public/img/banner-home.jpg') }}"></div>

      <div class="banner-content wow fadeInUp">
        <h2 class="title"><b>Soluções gastronômicas</b> para sua empresa. <br>É só se servir e <b>bom apetite!</b></h2>
      </div>
    </section>

    <section class="products">
      <div class="container">
        <div class="row">

          <div class="wow fadeInUp">
            <div class="pre-frame">
              <img src="{{ url('public/img/cozzi-icon.png') }}" alt="Ícone Cozzi Restaurantes Industriais">
            </div>
          </div>

          <h2 class="title wow fadeInUp">Nossos <b>produtos</b></h2>

          <!-- each product -->
          <a href="{{ url('/') }}" class="card col-sm-4 col-lg-1 wow fadeInUp">
            <div class="caption-top">
              <img src="{{ url('public/img/produtos/produto-logo-hover.png') }}" alt="Logo do Produto">
            </div>
            <figure>
              <img src="{{ url('public/img/produtos/produto-1.jpg') }}" alt="Produto 01" class="thumb">
            </figure>
            <div class="caption-bottom">
              <img src="{{ url('public/img/produtos/produto-logo.png') }}" alt="Logo do Produto">
              <div class="card-cta">
                <p>Tudo rápido e gostoso</p>
                <span class="btn btn-more orange">Saiba mais <i class="fa fa-arrow-right"></i></span>
              </div>
            </div>
          </a>
          <!-- each product -->

          
          <a href="{{ url('/') }}" class="card col-sm-4 col-lg-1 wow fadeInUp">
            <div class="caption-top">
              <img src="{{ url('public/img/produtos/produto-logo-hover.png') }}" alt="Logo do Produto">
            </div>
            <figure>
              <img src="{{ url('public/img/produtos/produto-3.jpg') }}" alt="Produto 01" class="thumb">
            </figure>
            <div class="caption-bottom">
              <img src="{{ url('public/img/produtos/produto-logo.png') }}" alt="Logo do Produto">
              <div class="card-cta">
                <p>Tudo rápido e gostoso</p>
                <span class="btn btn-more orange">Saiba mais <i class="fa fa-arrow-right"></i></span>
              </div>
            </div>
          </a>
          
          <a href="{{ url('/') }}" class="card col-sm-4 col-lg-1 wow fadeInUp">
            <div class="caption-top">
              <img src="{{ url('public/img/produtos/produto-logo-hover.png') }}" alt="Logo do Produto">
            </div>
            <figure>
              <img src="{{ url('public/img/produtos/produto-4.jpg') }}" alt="Produto 01" class="thumb">
            </figure>
            <div class="caption-bottom">
              <img src="{{ url('public/img/produtos/produto-logo.png') }}" alt="Logo do Produto">
              <div class="card-cta">
                <p>Tudo rápido e gostoso</p>
                <span class="btn btn-more orange">Saiba mais <i class="fa fa-arrow-right"></i></span>
              </div>
            </div>
          </a>
          
          <a href="{{ url('/') }}" class="card col-sm-4 col-lg-1 wow fadeInUp">
            <div class="caption-top">
              <img src="{{ url('public/img/produtos/produto-logo-hover.png') }}" alt="Logo do Produto">
            </div>
            <figure>
              <img src="{{ url('public/img/produtos/produto-1.jpg') }}" alt="Produto 01" class="thumb">
            </figure>
            <div class="caption-bottom">
              <img src="{{ url('public/img/produtos/produto-logo.png') }}" alt="Logo do Produto">
              <div class="card-cta">
                <p>Tudo rápido e gostoso</p>
                <span class="btn btn-more orange">Saiba mais <i class="fa fa-arrow-right"></i></span>
              </div>
            </div>
          </a>

          <a href="{{ url('/') }}" class="btn btn-large gray wow fadeInUp">Conheça todos os nossos produtos</a>
          
        </div>
      </div>
    </section>

    <section class="spotlight">
      <div class="container">
        <div class="row">

          <div class="spot col-sm-4 col-lg-2 wow fadeInUp">
            <h3 class="title"><span class="ilustration wow showSmoke"></span>Conheça <b>nossas campanhas</b></h3>

            <a href="{{ url('/') }}" class="spot-article">
              <div class="thumb js-fill">
                <img src="{{ url('public/img/nossas-campanhas.jpg') }}" alt="Nossas campahas">
              </div>
              <div class="spot-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam placerat tempor sem in eleifend. <b>Aenean facilisis accumsan augue.</b> Sed a enim aliquet, tempus urna vitae, eleifend nunc.</p>
                <span class="btn btn-more red">Saiba mais <i class="fa fa-arrow-right"></i></span>
              </div>
            </a>
          </div>

          <div class="spot col-sm-4 col-lg-2 wow fadeInUp">
            <h3 class="title"><span class="ilustration wow showSmoke"></span>Com <b>sabor</b></h3>

            <a href="{{ url('/') }}" class="spot-article">
              <div class="thumb js-fill">
                <img src="{{ url('public/img/com-sabor.jpg') }}" alt="Com sabor">
              </div>
              <div class="spot-content">
                <p><b>Conheça também nosso produto inteligente e ainda mais acessível.</b>A Cozzi também possui uma opção de produto para empresas que levem em consideração a inteligência operacional do serviço, como a otimização de insumos.</p>
                <span class="btn btn-more red">Saiba mais <i class="fa fa-arrow-right"></i></span>
              </div>
            </a>
          </div>

        </div>
      </div>
    </section>

    <section class="news">

      <a href="{{ url('/') }}" class="news-spotlight">
        <figure class="js-fill">
          <img src="{{ url('public/img/news-thumb.jpg') }}" alt="Notícia em destaque">
          <figcaption>
            <span class="date">00.00.0000</span><br>
            <p><b>Lorem ipsum dolor sit amet</b>, consectetur adipiscing elit aliquam placerat tempor sem in eleifend facilisis accumsan augue.</p>
          </figcaption>
        </figure>
      </a>

      <div class="recent-news">
        <h2 class="title wow fadeInUp">Últimas <b>notícias</b></h2>
        <a href="{{ url('/') }}" class="btn btn-more orange wow fadeInUp">Ver todas <i class="fa fa-arrow-right"></i></a>

        <ul class="news-list">
          <!-- each -->
          <li class="news-item wow fadeInUp">
            <a href="{{ url('/') }}">
              <span class="date">00.00.0000</span><br>
              <p><b>Lorem ipsum dolor sit amet</b>, consectetur adipiscing elit aliquam placerat tempor sem in eleifend facilisis accumsan augue.</p>
            </a>
          </li>
          <!-- each -->

          <li class="news-item wow fadeInUp">
            <a href="{{ url('/') }}">
              <span class="date">00.00.0000</span><br>
              <p><b>Lorem ipsum dolor sit amet</b>, consectetur adipiscing elit aliquam placerat tempor sem in eleifend facilisis accumsan augue.</p>
            </a>
          </li>
          <li class="news-item wow fadeInUp">
            <a href="{{ url('/') }}">
              <span class="date">00.00.0000</span><br>
              <p><b>Lorem ipsum dolor sit amet</b>, consectetur adipiscing elit aliquam placerat tempor sem in eleifend facilisis accumsan augue.</p>
            </a>
          </li>
          <li class="news-item wow fadeInUp">
            <a href="{{ url('/') }}">
              <span class="date">00.00.0000</span><br>
              <p><b>Lorem ipsum dolor sit amet</b>, consectetur adipiscing elit aliquam placerat tempor sem in eleifend facilisis accumsan augue.</p>
            </a>
          </li>
        </ul>
      </div>

    </section>

  </div>
  {{--  #home  --}}

@endsection


@section('assets')

  {{--  style  --}}
  <link rel="stylesheet" href="{{ url('public/css/pages/home.css') }}">

  {{--  scripts  --}}
  <script src="{{ url('public/js/libs/jquery.min.js') }}"></script>
  <script src="{{ url('public/js/libs/parallax.min.js') }}"></script>
  <script src="{{ url('public/js/libs/wow.min.js') }}"></script>
  <script src="{{ url('public/js/libs/owl.carousel.min.js') }}"></script>
  <script src="{{ url('public/js/libs/imagesloaded.pkgd.min.js') }}"></script>
  <script src="{{ url('public/js/libs/imagefill.js') }}"></script>

  <script src="{{ url('public/js/scripts.js') }}"></script>
  <script src="{{ url('public/js/pages/home.js') }}"></script>

@endsection