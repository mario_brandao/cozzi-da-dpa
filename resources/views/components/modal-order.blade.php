<div id="order-modal" class="modal">
  <div class="m-overlay js-close-modal"></div>

  <div class="m-container">
    <button class="btn btn-m-close js-close-modal" title="Fechar"><i class="icon i-font i-close"></i></button>

    <div class="m-content">

      <p class="m-title">Confirmação de pedido</p>

      <div class="col-sm-4 col-lg-10 offset-lg-1">
        <div class="order-table">
          <table>
            <thead>
              <tr>
                <th><i class="icon i-font i-products"></i><span> Produtos</span></th>
                <th><i class="icon i-font i-units"></i><span> Unidades</span></th>
                <th><i class="icon i-font i-boxes"></i><span> Caixas</span></th>
                <th><i class="icon i-font i-balance"></i><span> Kg</span></th>
                <th><span>(R$) Valor total <br>por produto</span></th>
              </tr>
            </thead>
  
            <tbody>
              <tr>
                <td>Nome do produto</td>
                <td>10</td>
                <td>100</td>
                <td>1500</td>
                <td><span class="tiny">R$</span><div class="price">1050,50</div></td>
              </tr>
              <tr>
                <td>Nome do produto</td>
                <td>10</td>
                <td>100</td>
                <td>1500</td>
                <td><span class="tiny">R$</span><div class="price">1050,50</div></td>
              </tr>
              <tr>
                <td>Nome do produto</td>
                <td>10</td>
                <td>100</td>
                <td>1500</td>
                <td><span class="tiny">R$</span><div class="price">1050,50</div></td>
              </tr>
            </tbody>
          </table>
  
          <div class="tfooter">
            <p>Total</p>
            <p><span class="tiny">R$</span> <span class="js-order-total"></span></p>
          </div>
        </div>
      </div>

      <div class="controls mobile-centerd">
        <button class="btn btn-red smaller js-close-modal">Voltar ao carrinho</button>
        <button class="btn btn-green smaller js-send-order">Confirmar pedido</button>
      </div>

    </div>
  </div>
</div>