<div id="amount-modal" class="modal">
  <div class="m-overlay js-close-modal"></div>

  <div class="m-container">
    <button class="btn btn-m-close js-close-modal" title="Fechar"><i class="icon i-font i-close"></i></button>

    <div class="m-content">
      <h3 class="m-title"><i class="icon i-font i-products"></i> <span class="js-prod-name">Abacaxi</span></h3>

      <form action="#" method="POST" class="form-product">

        <div class="row">
          <div class="amount-type col-sm-4 col-lg-2 offset-lg-2" data-type="unidade">{{--  default  --}}
            <div class="fake-checkbox"><i class="icon i-font i-check-box"></i></div>
            <div class="check-label">Unidade <br>(R$<span class="single-price js-unity-price"></span>)</div>
            <button type="button" class="btn js-edit-amount" title="Editar"><i class="icon i-font i-edit"></i></button>
            
            <div class="selected-amount">
              <i class="icon i-font i-units"></i> <span class="box-amount"></span><br>
              <span class="amount-price">R$</span>
            </div>
          </div>
  
          <div class="amount-type col-sm-4 col-lg-2 offset-lg-1" data-type="caixa">{{--  .checked  --}}
            <div class="fake-checkbox"><i class="icon i-font i-check-box"></i></div>
            <div class="check-label">Caixa <br>(R$<span class="single-price js-box-price"></span>)</div>
            <button type="button" class="btn js-edit-amount" title="Editar"><i class="icon i-font i-edit"></i></button>
  
            <div class="selected-amount">
              <i class="icon i-font i-boxes"></i> <span class="box-amount"></span><br>
              <span class="amount-price">R$</span>
            </div>
          </div>
          
          <div class="amount-type col-sm-4 col-lg-2 offset-lg-1" data-type="quilo">{{--  .current  --}}
            <div class="fake-checkbox"><i class="icon i-font i-check-box"></i></div>
            <div class="check-label">Peso (kg) <br>(R$<span class="single-price js-balance-price"></span>)</div>
            <button type="button" class="btn js-edit-amount" title="Editar"><i class="icon i-font i-edit"></i></button>
            
            <div class="selected-amount">
              <i class="icon i-font i-balance"></i> <span class="box-amount"></span><br>
              <span class="amount-price">R$</span>
            </div>
          </div>

          <div class="amount-field col-sm-4 col-lg-2 offset-lg-5">
            <p>Digite a quantidade desejada</p>
            <input type="number" name="currentValue" class="js-amount-field" autocomplete="off">
          </div>

          <div class="col-sm-4 col-lg-6 offset-lg-3">
            <div class="seekbar">
              <span class="rails"></span>
            </div>
          </div>
        </div>

        <div class="controls">
          <button class="btn btn-green smaller">Confirmar quantidades</button>
        </div>

      </form>
    </div>
  </div>
</div>