<div id="photo-modal" class="modal m-file">
  <div class="m-overlay js-close-modal"></div>

  <div class="m-container">
    <button class="btn btn-m-close js-close-modal" title="Fechar"><i class="icon i-font i-close"></i></button>

    <div class="m-content">

      <div class="file-draggable">
        <div class="js-upload-draggable"></div>
      </div>

    </div>
  </div>
</div>