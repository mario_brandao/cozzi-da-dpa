<div id="address-modal" class="modal">
  <div class="m-overlay js-close-modal"></div>

  <div class="m-container">
    <button class="btn btn-m-close js-close-modal" title="Fechar"><i class="icon i-font i-close"></i></button>

    <div class="m-content">

      <p class="m-title">Escolha o endereço de entrega</p>

      <div class="chosen-wrapper col-sm-4 col-lg-8 offset-lg-2">
        <label for="address">Endereço</label>

        <select name="address" id="address" class="chosen-field js-address-field" required>
          <option value="" disabled>Selecione um endereço</option>
          <option value="1" selected>Opção 1</option>
          <option value="2">Opção 2</option>
        </select>
      </div>

      <div class="controls mobile-centerd">
        <button class="btn btn-red smaller js-close-modal">Editar endereços</button>
        <button class="btn btn-green smaller js-set-address">Confirmar endereço</button>
      </div>

    </div>
  </div>
</div>