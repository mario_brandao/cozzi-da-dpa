<div id="edit-address-modal" class="modal">
  <div class="m-overlay js-close-modal"></div>

  <div class="m-container">
    <button class="btn btn-m-close js-close-modal" title="Fechar"><i class="icon i-font i-close"></i></button>

    <div class="m-content">

      <form action="#" method="POST" class="address-form">
        <div class="col-sm-4 col-lg-3 offset-lg-1">
          <div class="label-wrapper">
            <span for="cep">CEP</span>
            <input type="text" name="cep" id="cep" class="js-cep" required>
          </div>
        </div>
  
        <div class="col-sm-4 col-lg-7">
          <div class="label-wrapper">
            <span for="logradouro">Logradouro</span>
            <input type="text" name="logradouro" id="logradouro" class="place-inputs" required>
          </div>
        </div>
        
        <div class="col-sm-4 col-lg-2 offset-lg-1">
          <div class="label-wrapper">
            <span for="numero">Número</span>
            <input type="text" name="numero" id="numero" class="place-inputs" required>
          </div>
        </div>
        
        <div class="col-sm-4 col-lg-3">
          <div class="label-wrapper">
            <span for="bairro">Bairro</span>
            <input type="text" name="bairro" id="bairro" class="place-inputs" required>
          </div>
        </div>
  
        <div class="col-sm-4 col-lg-3">
          <div class="label-wrapper">
            <span for="cidade">Cidade</span>
            <input type="text" name="cidade" id="cidade" class="place-inputs" required>
          </div>
        </div>
  
        <div class="col-sm-4 col-lg-2">
          <div class="label-wrapper">
            <span for="uf">UF</span>
            <input type="text" name="uf" id="estado" class="place-inputs" required>
          </div>
        </div>
  
        <div class="controls">
          <button class="btn btn-red smaller js-close-modal">Cancelar</button>
          <button class="btn btn-green smaller js-save-address">Salvar endereço</button>
        </div>
      </form>

    </div>
  </div>
</div>