// ================================
// Aifrutas
// Made by Ativotech
// Enjoy ;)
// ================================

mobile = $(window).width() <= 1023 ? true : false;

$(document).ready(function() {

  $(window).on('load', function() {
    $('.preloader').fadeOut();
    $(window).on('beforeunload', function () {
      $('.preloader').fadeIn();
    });

    // WOW
    var wow = new WOW ({
      boxClass:     'wow',
      animateClass: 'animated',
      mobile:       true,
      live:         true
    });
    wow.init();


    var menuFix = function () {
      var menu = $('header');
      var hOffset = 120;

      if (mobile) {
        hOffset = 400;
      } else {
        hOffset = 120;
      }

      function checkOffset() {
        var wTop = window.scrollY;

        if (wTop >= hOffset) {
          menu.addClass('fixed');
        } else {
          menu.removeClass('fixed');
        };
      };
      checkOffset();

      $(window).on('scroll', function () {
        checkOffset();
      });
    }();
  });
  
  // MENU Funcs
  $('.js-menu-toggle').on('click', function(e) {
    e.preventDefault();
    $(this).toggleClass('active');
    $('.menu-content').slideToggle();
  });

  $('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      768: {
        items: 3,
        margin: 15
      },
      1024: {
        items: 4
      },
      1900: {
        margin: 30
      }
    }
  });

  $(".owl-prev").html('<i class="fa fa-arrow-left"></i>');
  $(".owl-next").html('<i class="fa fa-arrow-right"></i>');
});